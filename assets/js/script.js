//step 1: check if traveler or not

//step 1.1 non-traveler
//trace() contact tracing
//history() if Yes check symptoms, if No cleared //since not traveler cleared
//symptoms
//PUI or PUM
//if PUI check severity 


//step 1.2 traveler
//subject() check travel history
//symptoms() if Yes check if PUI, if No PUM //subject for HQ
//PUI or PUM
//if PUI check for severity


//Entry Validation
// profile = {
// 	fName : prompt(`Enter first name`),
// 	lName : prompt(`Enter last name`),
// 	age : prompt(`How old are you?`),
// 	fullName : function() {
// 		return console.log(`${this.fName} ${this.lName} Age: ${this.age}`)
// 	},
// 	status : [{
// 		name : 'PUI',
// 		check : function() {
// 			return alert('Person Under Investigation')
// 		},
// 		severity : [{
// 			name: 'Mild',
// 			priority : function(){
// 				return alert(`refer to L2/L3 Hospital`)
// 			}
// 		},
// 		{
// 			name: 'Severe',
// 			priority : function(){
// 				return alert(`Check Referral Hospital`)
// 			}
// 		}]
// 	},
// 	{
// 		name : 'PUM',
// 		check : function() {
// 			return alert('Person Under Monitoring')
// 		}
// 	},
// 	{
// 		name : 'Healthy',
// 		check : function() {
// 			return alert(`Congratulations you are Healthy!`)
// 		}
// 	}]
// }




// let user = prompt(`Are you traveling?`)
// let country = prompt(`Which Country are you coming from?`)
// let contact = prompt(`What is your immediate mobile contact number?`)
// let email = prompt(`Enter email address.`)


// let indication = function() {
// 	let exhibit = prompt(`Is it severe cough?`)

// 	exhibit.toUpperCase('YES') == 'YES' ? alert(profile.status[0].severity[1].priority())
// 	: alert(profile.status[0].severity[0].priority()) 
// }

// //check for history/contact tracing
// let history = function() {
// 	return prompt(`
// 	Are you:
// 	A. Providing direct care for COVID-19 patient?
// 	B. Working together or staying in the same close environment of a COVID-19?
// 	C. Traveling together with COVID-19 patient in any kind of conveyance?
// 	D. Living in the same household as a COVID-19 patient within a 14-day period?`)
// }

// let symptoms = function(){
// 	let temp = prompt(`what's your body temperature?`)
// 	let respiratoryIllness = prompt(`are you exhibiting cough and/or colds?`) 

// 	//check if PUI or PUM
// 	temp >= 38 && respiratoryIllness.toUpperCase(`YES`) == 'YES' ? indication()//check severity
// 	: alert(profile.status[1].check())
// }


// // if non-traveler
// // contact tracing
// let trace = function(){
// 	history()

// 	history().toUpperCase('YES') == 'YES' ? symptoms() //check symptoms
// 	: alert(profile.status[2].check())	
// }

// // if traveler
// let subject = function(){
// 	let days = prompt(`How many days in Travel?`)

// 	days >= 14 && country.toUpperCase('CHINA') == 'CHINA' || history().toUpperCase('YES') == 'YES' ? symptoms()//check symptoms
// 	: alert(profile.status[1].check()) //since traveler subject for HQ
// }


// //flow

// // 1. check if traveler or non-traveler
// user.toUpperCase('YES') == 'YES' || country.toUpperCase('CHINA') == 'CHINA' ? subject() //check symptoms
// : trace()


//niels approach

let fev, ill, screen, screen2, screen3,screen4;
const covid = [
	'yes',
	'no',
	{
		non:{
			providing: '\nExposure:\n\nProviding direct care for COVID-19 patient',
			working: '\nExposure:\n\nWorking together or staying in the same close environment of a COVID-19',
			travelling: '\nExposure:\n\nTravelling together with COVID-19 patient in any kind of conveyance',
			living: '\nExposure:\n\nLiving in the same household as a COVID-19 patient within a 14-day period',
		}
	},
	{
		symptoms: {
			fever: 'Had fever( >= 38.0 °C )',
			illness: 'Respiratory illness (cough and/or cold)'
		}
	},
	{
		status:{
			severe: 'Patient Under Investigation (PUI)',
			// mild: '\n\nPatient Under Investigation(PUI)\nMild manifestations, refer to L2/L3 Hospitals',
			pum: 'Patient Under Monitoring (PUM)\nHome Quarantine',
			negative: 'Not PUI OR PUM\nManage appropriately'
		}
	},
	'? Yes or No?',
    '\nSymptom:\n\n',
    '\nResult:\n\n',
    'In the past 14 days have you been in an area where COVID-19 is widespread'
];

function valFever(){
	while(true){ 
		fev = prompt(covid[6]+covid[3].symptoms.fever + covid[5]);
		if (fev == null) {
            return true;
        }
        else{
        	if (fev.length<=0 || !isNaN(fev) ) {
                alert("Please just answer Yes or No");
            } 
            else if(fev.toLowerCase() === covid[0] || fev.toLowerCase() == covid[1]){
            	return fev;
            }
            else {
                alert("Please just answer Yes or No");
            }
        }                    
    }
}
function valIllness(){
	while(true){ 
		ill = prompt(covid[6]+covid[3].symptoms.illness + covid[5]);
		 	
		if (ill == null) {
            return true;
        }
        else{
        	if (ill.length<=0 || !isNaN(ill) ) {
                alert("Please just answer Yes or No");
            } 
            else if(ill.toLowerCase() === covid[0] || ill.toLowerCase() == covid[1]){
            	return ill;
            }
            else {
                alert("Please just answer Yes or No");
            }
        }                    
    }
}
function valProviding(){
	while(true){ 
		screen = prompt(covid[2].non.providing + covid[5]);
		 	
		if (screen == null) {
            return true;
        }
        else{
        	if (screen.length<=0 || !isNaN(screen) ) {
                alert("Please just answer Yes or No");
            } 
            else if(screen.toLowerCase() === covid[0] || screen.toLowerCase() == covid[1]){
            	return screen;
            }
            else {
                alert("Please just answer Yes or No");
            }
        }                    
    }
}
function valWorking(){
	while(true){ 
		screen2 = prompt(covid[2].non.working + covid[5]);
		 	// 
		if (screen2 == null) {
            return true;
        }
        else{
        	if (screen2.length<=0 || !isNaN(screen2) ) {
                alert("Please just answer Yes or No");
            } 
            else if(screen2.toLowerCase() === covid[0] || screen2.toLowerCase() == covid[1]){
            	return screen2;
            }
            else {
                alert("Please just answer Yes or No");
            }
        }                    
    }
}
function valTravelling(){
	while(true){ 
		screen3 = prompt(covid[2].non.travelling + covid[5]);

		if (screen3 == null) {
            return true;
        }
        else{
        	if (screen3.length<=0 || !isNaN(screen3)) {
                alert("Please just answer Yes or No");
            } 
            else if(screen3.toLowerCase() === covid[0] || screen3.toLowerCase() == covid[1]){
            	return screen3;
            }
            else {
                alert("Please just answer Yes or No");
            }
        }                    
    }
}
function valLiving(){
	while(true){ 
		screen4 = prompt(covid[2].non.living + covid[5]);
		if (screen4 == null) {
            return true;
        }
        else{
        	if (screen4.length<=0 || !isNaN(screen4)) {
                alert("Please just answer Yes or No");
            } 
            else if(screen4.toLowerCase() === covid[0] || screen4.toLowerCase() == covid[1]){
            	return screen4;
            }
            else {
                alert("Please just answer Yes or No");
            }
        }                    
    }
}

function screenTool(){
	while(true){ 
		let traveller = prompt('\nTravel History:\n\n'+covid[8]+covid[5]);  	  
		
		if (traveller == null) {
            return true;
        }
        else{
        	if (traveller.length<=0 || !isNaN(traveller) ) {
                alert("Please just answer Yes or No");
            } 
            else if(traveller.toLowerCase() === covid[0]){
            	valFever();
            	valIllness();
            	if (fev.toLowerCase() === covid[0] && ill.toLowerCase() === covid[0] || fev.toLowerCase() === covid[0] && ill.toLowerCase() === covid[1] || fev.toLowerCase() === covid[1] && ill.toLowerCase() === covid[0]) {
            		return alert(covid[7]+covid[4].status.severe);
            	}
            	else{
            		return alert(covid[7]+covid[4].status.pum);
            	}
            }
            else if(traveller.toLowerCase() == covid[1]){
            	valProviding();
            	valWorking();
            	valTravelling();
            	valLiving();

            	if(screen.toLowerCase() === covid[0]){
            		valFever();
            		valIllness();
            		// screen = prompt(covid[2].non.providing+': Yes');

            		if (fev.toLowerCase() === covid[0] && ill.toLowerCase() === covid[0] || fev.toLowerCase() === covid[0] && ill.toLowerCase() === covid[1] || fev.toLowerCase() === covid[1] && ill.toLowerCase() === covid[0]) {
            			return alert(covid[7]+covid[4].status.severe);
            		}
            		else{
            			return alert(covid[7]+covid[4].status.pum);
            		}
            	}
            	else if(screen2.toLowerCase() === covid[0]){
            		valFever();
            		valIllness();

            		if (fev.toLowerCase() === covid[0] && ill.toLowerCase() === covid[0] || fev.toLowerCase() === covid[0] && ill.toLowerCase() === covid[1] || fev.toLowerCase() === covid[1] && ill.toLowerCase() === covid[0]) {
            			return alert(covid[7]+covid[4].status.severe);
            		}
            		else{
            			return alert(covid[7]+covid[4].status.pum);
            		}
            	}
            	else if(screen3.toLowerCase() === covid[0]){
            		valFever();
            		valIllness();

            		if (fev.toLowerCase() === covid[0] && ill.toLowerCase() === covid[0] || fev.toLowerCase() === covid[0] && ill.toLowerCase() === covid[1] || fev.toLowerCase() === covid[1] && ill.toLowerCase() === covid[0]) {
            			return alert(covid[7]+covid[4].status.severe);
            		}
            		else{
            			return alert(covid[7]+covid[4].status.pum);
            		}
            	}
            	else if(screen4.toLowerCase() === covid[0]){
            		valFever();
            		valIllness();

            		if (fev.toLowerCase() === covid[0] && ill.toLowerCase() === covid[0] || fev.toLowerCase() === covid[0] && ill.toLowerCase() === covid[1] || fev.toLowerCase() === covid[1] && ill.toLowerCase() === covid[0]) {
            			return alert(covid[7]+covid[4].status.severe);
            		}
            		else{
            			return alert(covid[7]+covid[4].status.pum);
            		}
            	}
                else if(screen.toLowerCase() === covid[0] || screen2.toLowerCase() === covid[0] || screen3.toLowerCase() === covid[0] || screen4.toLowerCase() === covid[0]){
                    valFever();
                    valIllness();

                    if (fev.toLowerCase() === covid[0] && ill.toLowerCase() === covid[0] || fev.toLowerCase() === covid[0] && ill.toLowerCase() === covid[1] || fev.toLowerCase() === covid[1] && ill.toLowerCase() === covid[0]) {
                        return alert(covid[7]+covid[4].status.severe);
                    }
                    else{
                        return alert(covid[7]+covid[4].status.pum);
                    }
                }
            	else{
            		return alert(covid[7]+covid[4].status.negative);
            	}

            }
            else {
               
                alert("Please just answer Yes or No");
            }
        }                    
    }                     
}
